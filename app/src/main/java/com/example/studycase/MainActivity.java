package com.example.studycase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText a, t;
    private TextView hasil;
    private Button cek;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        a= (EditText) findViewById(R.id.alas);
        t = (EditText) findViewById(R.id.tinggi);
        hasil = (TextView) findViewById(R.id.hasil);
        cek = (Button) findViewById(R.id.button_cek);

        cek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int alas = Integer.parseInt(a.getText().toString());
                int tinggi = Integer.parseInt(t.getText().toString());
                int luas = alas * tinggi;
                hasil.setText(luas + "");
            }
        });
    }
}
